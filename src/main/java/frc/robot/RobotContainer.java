// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.OperateCollector;
import frc.robot.commands.OperateDrive;
import frc.robot.commands.ShotCommand;
import frc.robot.subsystems.CollectorUtil;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.ShotUtil;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a "declarative" paradigm, very little robot logic should
 * actually be handled in the {@link Robot} periodic methods (other than the
 * scheduler calls). Instead, the structure of the robot (including subsystems,
 * commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // Subsystems
  private final DriveUtil driveUtil = new DriveUtil();
  private final ShotUtil shotUtil = new ShotUtil();
  private final CollectorUtil collectorUtil = new CollectorUtil();

  // Commands
  private final OperateDrive operateDrive = new OperateDrive(driveUtil);
  private final OperateCollector operateCollector = new OperateCollector(collectorUtil);
  // private final ShotCommand shotCommand = new ShotCommand(collectorUtil);
  // private final OperateShot operateShot = new OperateShot(shotUtil);

  // Joysticks and controllers
  private static XboxController driver, operator;

  // JoystickButton objects
  private JoystickButton runShotMotor, toggleCollector, shoot;

  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    driver = new XboxController(Constants.driver);
    operator = new XboxController(Constants.operator);
    
    // Configure the button bindings
    configureButtonBindings();

    // Configure the default commands
    configureSubsystemDefaultCommands();
  }

  /**
   * Use this method to define your button->command mappings.
   */
  private void configureButtonBindings() {
    // Initialize JoystickButton objects
    runShotMotor = new JoystickButton(driver, Button.kA.value);
    toggleCollector = new JoystickButton(driver, Button.kY.value);
    shoot = new JoystickButton(driver, Button.kX.value);
    // Define what each JoystickButton does
    runShotMotor.whenPressed(new InstantCommand(() -> shotUtil.toggleShotMotor(), shotUtil));
    toggleCollector.whenPressed(new InstantCommand(() -> collectorUtil.toggleCollector(), collectorUtil));
    shoot.whileHeld(new ShotCommand(collectorUtil));
  }

  /**
   * Use this method to define your subsystem's default commands.
   */
  private void configureSubsystemDefaultCommands() {
    driveUtil.setDefaultCommand(operateDrive);
    collectorUtil.setDefaultCommand(operateCollector);
  }

  /*
   * Controller get() methods - returns numerical value of axis
   */
  // Driver controller
  public static double getDriverLeftTrigger() {
    return driver.getLeftTriggerAxis();
  }
  
  public static double getDriverRightTrigger() {
    return driver.getRightTriggerAxis();
  }

  public static double getDriverLeftJoystickX() {
    return driver.getLeftX();
  }

  public static double getDriverLeftJoystickY() {
    return driver.getLeftY();
  }

  public static double getDriverRightJoystickX() {
    return driver.getRightX();
  }

  public static double getDriverRightJoystickY() {
    return driver.getRightY();
  }

  // Operator controller
  public static double getOperatorLeftTrigger() {
    return operator.getLeftTriggerAxis();
  }
  
  public static double getOperatorRightTrigger() {
    return operator.getRightTriggerAxis();
  }

  public static double getOperatorLeftJoystickX() {
    return operator.getLeftX();
  }

  public static double getOperatorLeftJoystickY() {
    return operator.getLeftY();
  }

  public static double getOperatorRightJoystickX() {
    return operator.getRightX();
  }

  public static double getOperatorRightJoystickY() {
    return operator.getRightY();
  }



  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    return null;
  }
}
