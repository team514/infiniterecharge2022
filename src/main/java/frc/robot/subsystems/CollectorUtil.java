// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class CollectorUtil extends SubsystemBase {
  private DoubleSolenoid collectorSolenoid;
  private TalonSRX intakeMotor;
  private final WPI_VictorSPX belt;
  private final Solenoid shotPiston;
  private final DigitalInput bannerSensor;

  public CollectorUtil() {
    collectorSolenoid = new DoubleSolenoid(Constants.PCM, Constants.forwardSolenoid, Constants.reverseSolenoid);
    intakeMotor = new TalonSRX(Constants.intakeMotor);
    intakeMotor.setInverted(true);
    belt = new WPI_VictorSPX(Constants.belt);
    belt.setInverted(true);
    shotPiston = new Solenoid(Constants.PCM, Constants.beltPiston);
    bannerSensor = new DigitalInput(Constants.bannerSensor);
  }

  public void toggleCollector() {
    if(collectorSolenoid.get().equals(Value.kForward)) {
      collectorSolenoid.set(Value.kReverse);
      runBelt(0);
    } else {
      collectorSolenoid.set(Value.kForward);
      runBelt(1.0);
    }
  }

  public boolean isCollectorDeployed() {
    return collectorSolenoid.get().equals(Value.kForward);
  }

  public void setIntakeSpeed(boolean enabled) {
    if (enabled) {
      intakeMotor.set(ControlMode.PercentOutput, 0.8);
    } else {
      intakeMotor.set(ControlMode.PercentOutput, 0.0);
    }
  }

  public void runBelt(double speed) {
    belt.set(speed);
  }

  public void deployPiston() {
    shotPiston.set(true);
  }

  public void retractPiston() {
    shotPiston.set(false);
  }

  public boolean isMagFilled() {
    return bannerSensor.get();
  }

  public boolean hasTension() {
    return shotPiston.get();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    SmartDashboard.putNumber("Intake Motor Speed", intakeMotor.getMotorOutputPercent());
    SmartDashboard.putBoolean("Is Collector Deployed", collectorSolenoid.get().equals(Value.kForward));

  }
}
